package com.company;
// Given a string, if the first or last chars are 'x', return the string without those 'x' chars,
// and otherwise return the string unchanged.
public class Main {

    public static void main(String[] args) {

    }
    public String withoutX(String str) {
        if (str.length() > 1) {
            if (str.charAt(0) == 'x') {
                str = str.substring(1);
            }
            if ((str.charAt(str.length()-1) == 'x')) {
                str = str.substring(0, str.length()-1);
            }
        }
        else if ((str.length() == 1)&&(str == "x")) {
            return "";
        }
        return str;
    }
}
