package com.company;
//Given a string, return a new string made of 3 copies of the last 2 chars of the original string.
//The string length will be at least 2.
public class Main {

    public static void main(String[] args) {
	// write your code here
    }
    public String extraEnd(String str) {
        String out = str.substring(str.length()-2);
        return out+out+out;
    }
}
